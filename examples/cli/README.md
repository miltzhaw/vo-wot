# Simple cli example
1. To run the example first create a virtual environment in python (the location of the virtual environment doesn't matter):
```bash
vo-wot$ python -m venv vo-wot-env
```

2. Activate the environment:
```bash
vo-wot$ source vo-wot-env/bin/activate
```

3. Install the python package:
```bash
vo-wot(env)$ pip install vo-wot
```

4. Run the example. The python package provides a cli named `vo-wot`. To use it run:
```py
vo-wot/examples/cli(env)$ vo-wot -t coffee-machine-td.json -f vo-descriptor.yaml coffee-machine-reduced.py
```

Alternatively the module can be directly called from the current directory using:
```py
vo-wot/examples/cli(env)$ python ../../wotpy/cli/cli.py -t coffee-machine-td.json -f vo-descriptor.yaml coffee-machine-reduced.py
```
